<?php
include("./templates/header.php");
?>

<div class="row">
  <div class="col-sm-6 col-md-4 col-md-offset-4">
    <div class="thumbnail">
      <img src="templates/logo.jpg" alt="...">
      <div class="caption">
        <h3 class='text-center'><strong>Connexion</strong></h3>
        <form action="./core/login.php" method="get">
          <input name='username' type="text" placeholder="Username">
          <label for='username'></label>
          <input name='password' type="password" placeholder="Password">
          <label for='password'></label>
      </div>
      <div class="modal-footer">
        <input class="btn btn-primary" type="submit" value='Log in'>
        <h4 class="text-center"> Don't have an account yet ? </h4>
        <a href='./templates/sign.php' title=''><button type="button" class="btn btn-default sign">Sign up</button></a>
      </div>
        <p><a href="#" class="btn btn-primary" role="button">Button</a>
      </div>
    </div>
  </div>
</div>

<?php include("./templates/footer.php"); ?>
